import cv2
import numpy as np
import matplotlib.pyplot as plt
from models.wpn.local_utils import detect_lp
from os.path import splitext, basename
from keras.models import model_from_json
import glob

class WPodNet:
    wpn_model = None

    def __init__(self):
        wpod_net_path = "models/wpn/wpod-net.json"
        wpod_net = self.load_model(wpod_net_path)


    def load_model(self, path):
        try:
            path = splitext(path)[0]
            with open('%s.json' % path, 'r') as json_file:
                model_json = json_file.read()
            model = model_from_json(model_json, custom_objects={})
            model.load_weights('%s.h5' % path)
            print("Loading model successfully...")
            self.wpn_model = model
            return model
        except Exception as e:
            print(e)

    def predict(self, image_path):
        def preprocess_image(image_path,resize=False):
            img = cv2.imread(image_path)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            img = img / 255
            if resize:
                img = cv2.resize(img, (224,224))
            return img

        def get_plate(image_path, Dmax=608, Dmin=256):
            vehicle = preprocess_image(image_path)
            ratio = float(max(vehicle.shape[:2])) / min(vehicle.shape[:2])
            side = int(ratio * Dmin)
            bound_dim = min(side, Dmax)
            _ , LpImg, _, cor = detect_lp(self.wpn_model, vehicle, bound_dim, lp_threshold=0.5)
            coords = []

            x_coordinates=cor[0][0]
            y_coordinates=cor[0][1]
            # store the top-left, top-right, bottom-left, bottom-right
            # of the plate license respectively
            for i in range(4):
                coords.append([int(x_coordinates[i]),int(y_coordinates[i])])

            return LpImg, cor, coords

        # Obtain plate image and its coordinates from an image

        LpImg,cor, coords = get_plate(image_path)
        # print("Detect %i plate(s) in"%len(LpImg))
        # print("Coordinate of plate(s) in image: \n", cor)
        # print (coords )
        return coords
