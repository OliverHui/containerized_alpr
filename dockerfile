FROM ubuntu:18.04
WORKDIR /
COPY . .
RUN apt-get update && apt-get install -y python3-pip git vim ffmpeg libsm6 libxext6 
ENV LANG=C.UTF-8
RUN pip3 install --upgrade pip
RUN pip3 install Cython
RUN pip3 install -r requirements.txt
EXPOSE 7000
CMD ["python3", "app.py"]
