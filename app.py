from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename
from models.wpn.wpodnet import WPodNet
from models.yolov3.yolov3 import YoloV3
from PIL import Image
from os import path
import json
import base64
import io
import requests

app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello World!"

@app.route("/process_image", methods=["POST"])
def message():
    #posted_data = request.get_json()

    res = json.loads(request.values['data'])
    file = request.files['upload']
    fn = secure_filename(file.filename)
    file.save(fn)
    model = res['model']

    if model == "wpodnet":
        model  = WPodNet()
    else:
         model = YoloV3()

    coords = model.predict(fn)
    #print (x)
    return jsonify(coords)#  main thread of execution to start the server


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7000)
