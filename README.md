## Build and run the container

```
docker build -t lp_detector .
docker run -p 7000:7000  lp_detector
```

## Query the server
<del>curl -X POST -F file=@1.jpg http://localhost:7000/process_image/yolo/</del>
```
curl -X POST http://localhost:7000/process_image -F upload=@test.jpg -F data="{\"model\":\"yolo\"}"
```
